# this is necessary to maintain compatibility with legacy builders, in this
# case, the py3 build macros in redhat packaging.

# https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html

from setuptools import setup

setup()
