============
ligo-gracedb
============
The Gravitational-wave Candidate Event Database (GraceDB) is a web service designed for aggregating and communicating information about candidate events from gravitational-wave searches and associated follow-ups.
``ligo-gracedb`` provides a Python-based client tool for facilitating interactions with the GraceDB API.


Documentation
-------------
For more information, see the `full documentation <https://ligo-gracedb.readthedocs.io/>`__.


Quick install
-------------
.. code:: python

    pip install ligo-gracedb


Contributing
------------
Please fork this `repository <https://git.ligo.org/computing/gracedb/client/>`__ and submit a merge request if you wish to contribute to this package.

See the `contributing documentation <https://ligo-gracedb.readthedocs.io//en/latest/contributing.html>`__ for more details.
